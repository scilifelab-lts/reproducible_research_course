<h1> Schedule </h1>

The course will take place in the room 10:an (see [travel info](travel.md)).

<table>
  <tr>
    <td colspan="3">
      <font size="4">
      <center> Day 1 - 2019-11-18 </center>
    </td>
  </tr>
  <tr>
    <td> <font size="3"><b>Time</b> </td>
    <td> <font size="3"><b>Topic</b> </td>
  </tr>
  <tr>
    <td> <font size="3"> 10:00
    <td> <font size="3"> Coffee and fika available from 10.00. Welcome!</td>
  </tr>
  <tr>
    <td> <font size="3"> 10:30
    <td> <font size="3"> Introduction to Reproducible Research<br>
        <font size="2">
        - <a href="https://drive.google.com/file/d/1U6-6ZEuCo5SIA9JLS11RAuDIzW1A0hin/view?usp=sharing">Introduction slides</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 11:15 </td>
    <td> <font size="3"> Data management and project organization <br>
        <font size="2">
        - <a href="https://drive.google.com/file/d/1McHlrR0L2i1Hl9pg5OqWSneAI4s3k2bP/view?usp=sharing">Data management slides</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 11:45  </td>
    <td> <font size="3"> Lunch </td>
  </tr>
  <tr>
    <td> <font size="3"> 12:45 </td>
    <td>
      <font size="3"> Master your dependencies - environments and reproducibility
      <font size="2"><i><br>
      - Introduction to the package and environment manager Conda<br>
      - Practical tutorial: <a href="../conda/">Conda</a> <br>
      - <a href="https://drive.google.com/file/d/1KkRSicN-7-PAz6H_Lub3QfUaFIUam05p/view?usp=sharing"> Conda slides</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 14:30  </td>
    <td> <font size="3"> Fika </td>
  </tr>
  <tr>
    <td> <font size="3"> 14:45 </td>
    <td>
      <font size="3"> Distributing and version tracking your code
      <font size="2"><i><br>
      - Introduction to version control and git<br>
      - Practical tutorial: <a href="../git/">Git</a></i> <br>
      - <a href="https://drive.google.com/open?id=1jtTobH-Wg_cGGbvPuYfyw0wXcL9D1wKZ"> Git slides</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 16:45 </td>
    <td> <font size="3"> Wrap-up day 1 <font size="2">(<a href="https://drive.google.com/file/d/17vODMbM3qthLa1vScQ2zC5v9ZX2SURUj/view?usp=sharing">slides</a>)</td>
  </tr>
  <tr>
    <td> <font size="3"> 17:00 </td>
    <td> <font size="3"> Free time!</td>
  </tr>
  <tr>
    <td> <font size="3"> 17:30 </td>
    <td> <font size="3"> Joint departure for dinner</td>
  </tr>
  <tr>
    <td> <font size="3"> 18:00 </td>
    <td> <font size="3"> <a href="../travel/">Course dinner</a></td>
  </tr>
  <tr>
    <td colspan="3"> </td>
  </tr>
  <tr>
    <td colspan="3">
      <font size="4">
      <center> Day 2  - 2019-11-19 </center>
    </td>
  </tr>
  <tr>
    <td> <font size="3"><b>Time</b> </td>
    <td> <font size="3"><b>Topic</b> </td>
  </tr>
  <tr>
    <td> <font size="3"> 09:00 </td>
    <td>
      <font size="3"> Organize your analysis using workflow managers
      <font size="2"><i><br>
      - Introduction to Snakemake<br>
      - Practical tutorial: <a href="../snakemake/">Snakemake</a></i><br>
      - <a href="https://drive.google.com/file/d/1vpIw0AL1DSzZL_HP1XulGj-iYUiQhMQO/view?usp=sharing"> Snakemake slides</a><br>
      <font size="3">Fika break ~10:15 <br>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 11:45  </td>
    <td> <font size="3"> Lunch </td>
  </tr>
  <tr>
    <td> <font size="3"> 12:45 </td>
    <td>
      <font size="2"><i><br>
      ...continued: Practical tutorial: <a href="../snakemake/">Snakemake</a></i><br><br>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 13:45 </td>
    <td>
      <font size="3"> Computational notebooks and reproducible reports
      <font size="2"><i><br>
      - Introduction to Jupyter and R Markdown<br>
      - Practical tutorial: <a href="../jupyter/">Jupyter</a><br>
      - Practical tutorial: <a href="../rmarkdown/">R Markdown</a></i><br>
      - <a href="https://drive.google.com/file/d/1umkIYGIOs0dOQKosp8IaWdeiBctN5aZS/view?usp=sharing">Computational notebooks slides</a>
      <br><br>
      <font size="3">Fika break ~14:30
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 16:45 </td>
    <td> <font size="3"> Wrap-up day 2 <font size="2"><a href="https://drive.google.com/file/d/1_FpE6QqQzT4Rc0bOlSk2FxzomJ_NJwPy/view?usp=sharing">(slides)</a></td>
  </tr>
  <tr>
    <td> <font size="3"> 17:00 </td>
    <td> <font size="3"> Free time! </td>
  </tr>
  <tr>
    <td> <font size="3"> 17:30 </td>
    <td> <font size="3"> Social event! </td>
  </tr>
  <tr>
    <td colspan="3"> </td>
  </tr>
  <tr>
    <td colspan="3">
      <font size="4">
      <center> Day 3  - 2019-11-20 </center>
    </td>
  </tr>
  <tr>
    <td> <font size="3"><b>Time</b> </td>
    <td> <font size="3"><b>Topic</b> </td>
  </tr>
  <tr>
    <td> <font size="3"> 09:00
    </td>
    <td>
      <font size="3"> Containerization
      <font size="2"><i><br>
      - Introduction to containers (<a href="https://drive.google.com/file/d/1Wfdrp6QTOQrxuwz1Pn7Cleiwmm5FgZwK/view?usp=sharing">slides</a>)<br>
      - Practical tutorial: <a href="../docker/">Docker</a><br>
      - Practical tutorial: <a href="../singularity">Singularity</a></i><br><br>
      <font size="3">Fika break ~10:15 <br>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 11:45  </td>
    <td> <font size="3"> Lunch </td>
  </tr>
  <tr>
    <td> <font size="3"> 12:45 </td>
    <td>
      <font size="3"> Wrap-up - putting the pieces together
      <font size="2"><i><br>
      - Overview - Using the tools in concert (<a href="https://drive.google.com/file/d/14g8x5w5kl17ccxVK5iGOOONbWBkbKoQ5/view?usp=sharing">slides</a>)<br>
      - Practical tutorial: <a href="../repres_project">Making a project reproducible</a></i>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 14:30  </td>
    <td> <font size="3"> Fika </td>
  </tr>
  <tr>
    <td> <font size="3"> 15:00  </td>
    <td>
      <font size="3"> Open discussion - Reproducible research in practice<br>
      <font size="2"><i>Tips and tricks on how to implement these procedures on a day-to-day basis<br>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 16:30 </td>
    <td> <font size="3"> All done! </td>
  </tr>
</table>

**Teachers:**  
Leif Wigge  
John Sundh  
Verena Kutschera  
Tomas Larsson
