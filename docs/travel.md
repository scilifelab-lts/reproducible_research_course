<h1> Finding the course venue </h1>

1. Transport yourself to Gothenburg (see e.g. [SJ](https://www.sj.se) or [MTR Express](https://www.mtrexpress.se)).
2. Go to Chalmers (Johanneberg campus) and locate the Chemistry building ([Kemigården 4](https://goo.gl/maps/yV6nqfX6a2SxfJqdA)). Nearby bus/tram stops include Chalmers, Kapellplatsen and Chalmers Tvärgata (search for public transport options [here](https://www.vasttrafik.se/en/)).
3. Enter the Chemistry building. Head left to find the stairwell and elevators (there are three stairwells in the Chemistry building, do not take the middle or right one). Go to the 10th floor. Note that the elevator goes to the 9th, and that you have to take the stairs the last floor to the 10th. If you are unable to use the stairs please contact the course leaders for assistance and an alternative route. Follow signs to get to the room 10:an ("tian").

![](images/kemihuset_map.jpg)

<h1> Finding the restaurant for the course dinner </h1>
The dinner will be at [Taverna Averna](https://www.tavernaaverna.se/) which is located at [Tredje Långgatan 7](https://goo.gl/maps/bJYBUVbmZHp3tC1d8).

<h1> Finding the "social event" venue </h1>

We will go to [Ölstugan Tullen](https://johanneberg.olstugan.se/) which is located at [Utlandagatan 14](https://goo.gl/maps/o61JyTmTEmPzxcHz6).
